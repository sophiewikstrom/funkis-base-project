const webpack = require("webpack");
const path = require("path");
const merge = require("webpack-merge");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

const common = require("./webpack.common.js");

console.log("Using production configuration of Webpack.");

module.exports = merge(common, {
  entry: ["./src/index.js"],
  mode: "production",
  module: {
    rules: [
      {
        test: /\.(less|css)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              url: true,
              sourceMap: false,
            },
          },
          {
            loader: "less-loader",
            options: {
              javascriptEnabled: true,
              relativeUrls: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "styles.css",
      chunkFilename: "styles-chunk.css",
    }),
  ],
  optimization: {
    minimizer: [
      new UglifyJSPlugin({
        uglifyOptions: {
          mangle: true,
          screwIE8: true,
          compress: {
            dead_code: true,
            conditionals: true,
            booleans: true,
            unused: true,
            if_return: true,
            join_vars: true,
            drop_console: true,
            drop_debugger: true,
            global_defs: {
              DEBUG: false,
            },
          },
          output: {
            comments: false,
          },
        },
      }),
    ],
  },
});
