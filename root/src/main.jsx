/*eslint-disable no-unused-vars*/
import babelPolyfill from "babel-polyfill";
/*eslint-enable no-unused-vars*/

import React from "react";
import createReactClass from "create-react-class";
import { Router, IndexRoute, Route, hashHistory } from "react-router";
import attachFastClick from "fastclick";
import classnames from "classnames";

import {
  FMInspectorContainer,
  FMGraphicItemContainer,
  FMChapter,
  FMLoader,
  FMResizableContainer,
  ApplicationConfigStore,
  ChapterStore,
  GraphicItemStore,
  KeyValueStore,
  LocationStore,
  SubtitleStore,
  SoundActions,
  MiscActions,
  ApplicationConfigActions,
  ChapterActions,
  GraphicItemActions,
  LocationActions,
  LocomotiveActions,
  MenuActions,
  NavigationActions,
  KeyValueActions,
  LoadXMLUtils,
  LoadJSONUtils,
  ProjectUtils,
  GraphicItemUtils,
  SuspendDataStoreObserver,
  Ajax,
  ChapterUtils,
  StatusUtils,
  LocalizationUtils,
} from "funkis-framework";

import TestArea from "./project/display/TestArea.jsx";

import graphicItems from "./project/graphicitems.js";

import FMInspectorContent from "./project/inspector/FMInspectorContent.jsx";

GraphicItemUtils.initialize(graphicItems);

window.SoundActions = SoundActions;
window.MiscActions = MiscActions;
window.ApplicationConfigActions = ApplicationConfigActions;
window.ChapterActions = ChapterActions;
window.GraphicItemActions = GraphicItemActions;
window.LocomotiveActions = LocomotiveActions;
window.MenuActions = MenuActions;
window.NavigationActions = NavigationActions;
window.LocationActions = LocationActions;

attachFastClick.attach(document.body);

LoadXMLUtils.getConfigJSON(ProjectUtils.getConfigFileUrl());

class FMProject extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: ApplicationConfigStore.getConfig(),
      maxWidth: 1230,
      minWidth: 975,
      maxHeight: 572,
      minHeight: 572,
      preloads: undefined,
    };
  }

  static defaultProps = {
    chapterId: "",
    sceneId: "",
  };

  componentDidMount = () => {
    LocationStore.addChangeListener(this._onLocationStoreChangeHandler);
    ApplicationConfigStore.addChangeListener(this._onApplicationConfigChange);
    SubtitleStore.addChangeListener(this._onSubtitleStoreChanged);
    Ajax.addChangeListener(this._onNetworkStatusChange);
    SuspendDataStoreObserver.addCourseStatusChangeListener(this._onSuspendDataStoreObserverCourseStatusChangeHandler);
    SuspendDataStoreObserver.addSuspendDataChangeListener(this._onSuspendDataStoreObserverSuspendDataChangeHandler);

    SuspendDataStoreObserver.addStore(
      "chapters",
      ChapterStore,
      store => {
        // Save function. Only save important stuff to savedata.
        var chapters = store.getAllChapters();
        return ChapterUtils.stripChaptersForSaveData(chapters);
      },

      data => {
        // Restore function
        ChapterActions.receiveSaveData(data);
      },

      store => {
        // isCompleted function. Checks if all chapters are completed.

        const chapters = store.getAllChapters();
        if (chapters.length === 0) {
          // No chapters are loaded yet. Return false in the mean time.
          return false;
        }

        return chapters.every(chapter => chapter.status === "COMPLETED");
      },

      true
    );

    SuspendDataStoreObserver.addStore(
      "keyValue",
      KeyValueStore,
      store => {
        return store.getKeyValueStore();
      },

      data => {
        KeyValueActions.setStore(data);
      }
    );
  };

  componentWillUnmount = () => {
    LocationStore.removeChangeListener(this._onLocationStoreChangeHandler);
    ApplicationConfigStore.removeChangeListener(this._onApplicationConfigChange);
    SubtitleStore.removeChangeListener(this._onSubtitleStoreChanged);
    Ajax.removeChangeListener(this._onNetworkStatusChange);
  };

  _onSuspendDataStoreObserverCourseStatusChangeHandler = newCourseStatus => {
    console.log("main.jsx: _onSuspendDataStoreObserverChangeHandler:", newCourseStatus);
    this.setState({ courseStatus: newCourseStatus });
  };

  _onSuspendDataStoreObserverSuspendDataChangeHandler = suspendData => {
    console.log("main.jsx: _onSuspendDataStoreObserverSuspendDataChangeHandler:", suspendData);
    this.setState({ suspendData: suspendData });
  };

  _onSubtitleStoreChanged = () => {
    this.forceUpdate();
  };

  _onApplicationConfigChange = () => {
    const prevConfig = this.state ? this.state.config : undefined;
    const config = ApplicationConfigStore.getConfig();

    this.setState({ config });

    if (prevConfig.language !== config.language) {
      ProjectUtils.loadLanguageSpecificFiles(config.projectPrefix, config.language);
    }

    if (prevConfig.projectPrefix !== config.projectPrefix) {
      LoadXMLUtils.getStructureXML(ProjectUtils.getStructureFileUrl(config.projectPrefix));

      // Use LoadJSONUtils to load project as JSON. Remove LoadXMLUtils.getStructureXML(...)
      /*
      LoadJSONUtils.getStructureJSON(ProjectUtils.getStructureFileJSONUrl(config.projectPrefix));
      LoadJSONUtils.getGraphicItemsJSON(ProjectUtils.getGraphicItemsFileJSONUrl(config.projectPrefix));
      LoadJSONUtils.getLocomotivesJSON(ProjectUtils.getLocomotivesFileJSONUrl(config.projectPrefix));
      */

      ProjectUtils.loadLanguageSpecificFiles(config.projectPrefix, config.language);

      LoadJSONUtils.getPreloadJSON(ProjectUtils.getPreloadFileJSONUrl(config.projectPrefix))
        .then(json => {
          this.setState({ preloads: json });
        })
        .catch(() => {
          this.setState({ preloads: [] });
        });
    }
  };

  _onLocationStoreChangeHandler = () => {
    const { chapterId, sceneId } = LocationStore.getCurrentLocation();
    this.setState({ chapterId, sceneId });
  };

  _onNetworkStatusChange = () => {
    var networkStatus = Ajax.getNetworkStatus();
    if (networkStatus !== ApplicationConfigStore.getConfig().networkStatus) {
      ApplicationConfigActions.update({ networkStatus: networkStatus });
      console.log("networkStatus:", networkStatus);
    }
  };

  render = () => {
    const { config, chapterId, sceneId } = this.state;
    var chapterObject = ChapterStore.getChapter(chapterId);

    if (!chapterObject) {
      chapterObject = ChapterStore.getChapter(ChapterStore.getRootChapterId());
    }

    if (!chapterObject || !this.state.preloads) {
      return <div />;
    }

    Object.assign(chapterObject, {
      currentSceneId: sceneId,
      courseStatus: this.state.courseStatus,
    });

    const globalGraphicItemIds = GraphicItemStore.getGlobalGraphicItems().map(graphicItem => graphicItem.id);

    var rtl =
      (config.devMode && config.devMode.enabled && config.devMode.rtl) ||
      config.rtl ||
      LocalizationUtils.isLanguageCodeRtl(config.language);

    var courseClassName = classnames(
      "fm-course",
      StatusUtils.convertStatusToClassName(this.state.courseStatus),
      config.language,
      {
        "dev-mode": config.devMode && config.devMode.enabled,
        "production-mode": config.devMode && !config.devMode.enabled,
        "dev-mode-graphic-item-border": config.devMode && config.devMode.graphicItemBorder,
        "dev-mode-video-border": config.devMode && config.devMode.videoBorder,
        "dev-mode-rtl": config.devMode && config.devMode.rtl,
        rtl: rtl,
        ltr: !rtl,
      }
    );

    var inspector = undefined;

    if (config.showInspector) {
      inspector = (
        <FMInspectorContainer
          courseStatus={this.state.courseStatus}
          suspendData={this.state.suspendData}
          currentChapterId={this.state.chapterId}
          currentSceneId={this.state.sceneId}
        >
          <FMInspectorContent />
        </FMInspectorContainer>
      );
    }
    return (
      <div className={courseClassName}>
        <FMLoader manifest={this.state.preloads}>
          <FMResizableContainer
            className="fm-outer-container"
            maxWidth={this.state.maxWidth}
            minWidth={this.state.minWidth}
            maxHeight={this.state.maxHeight}
            minHeight={this.state.minHeight}
            padding={20}
          >
            <div className="fm-inner-container">
              <FMChapter {...chapterObject} />
            </div>
            <FMGraphicItemContainer className="fm-global-graphic-items" graphicItemIds={globalGraphicItemIds} />
          </FMResizableContainer>

          {inspector}
        </FMLoader>
      </div>
    );
  };
}

var Root = createReactClass({
  render: function() {
    return <div>{this.props.children}</div>;
  },
});

var FMProjectRoute = createReactClass({
  componentDidMount: function() {
    const { chapterId, sceneId } = this.props.params;
    NavigationActions.navigateTo([chapterId, sceneId]);
  },

  render: function() {
    return (
      <div className="fm-project">
        <FMProject />
      </div>
    );
  },
});

var FMDevTestAreaRoute = createReactClass({
  render: function() {
    return (
      <div style={{ textAlign: "center" }}>
        <h2>Dev test area</h2>
        <TestArea />
      </div>
    );
  },
});

var FMProjectWithRoutes = () => {
  return (
    <Router history={hashHistory}>
      <Route path="/" component={Root}>
        <Route path="dev-test-area" component={FMDevTestAreaRoute} />
        <Route path=":chapterId/:sceneId" component={FMProjectRoute} />
        <Route path=":chapterId" component={FMProjectRoute} />
        <IndexRoute component={FMProjectRoute} />
      </Route>
    </Router>
  );
};

export default FMProjectWithRoutes;
