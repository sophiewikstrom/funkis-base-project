import React from "react";
import createReactClass from "create-react-class";

import toolStoreData from "../../../tool/toolStoreData.json";

import { FMStructureOverview } from "funkis-framework";

var StructureOverview = createReactClass({
  render: function() {
    var thumbnails = [];

    if (toolStoreData) {
      thumbnails = JSON.parse(JSON.stringify(toolStoreData)).thumbnails;
    }

    return <FMStructureOverview {...this.props} thumbnails={thumbnails} />;
  },
});

export default StructureOverview;
