const webpack = require("webpack");
const path = require("path");
const merge = require("webpack-merge");

const WebpackNotifierPlugin = require("webpack-notifier");
const ErrorOverlayPlugin = require("error-overlay-webpack-plugin");
const CaseSensitivePathsPlugin = require("case-sensitive-paths-webpack-plugin");
const WatchMissingNodeModulesPlugin = require("react-dev-utils/WatchMissingNodeModulesPlugin");
const eslintFormatter = require("react-dev-utils/eslintFormatter");

const common = require("./webpack.common.js");

console.log("Using development configuration of Webpack.");

module.exports = merge(common, {
  entry: ["react-hot-loader/patch", "./src/index.js"],
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx|mjs)$/,
        enforce: "pre",
        use: [
          {
            options: {
              formatter: eslintFormatter,
              eslintPath: require.resolve("eslint"),
              baseConfig: {
                extends: [require.resolve("eslint-config-react-app")],
                "jsx-a11y/href-no-hash": "off",
                rules: {
                  eqeqeq: "off",
                  "no-dupe-keys": "off",
                },
              },
              ignore: false,
              useEslintrc: false,
            },
            loader: require.resolve("eslint-loader"),
          },
        ],
      },
      {
        test: /\.(less|css)$/i,
        use: [
          { loader: "style-loader" },
          {
            loader: "css-loader",
            options: {
              url: true,
              sourceMap: true,
            },
          },
          {
            loader: "less-loader",
            options: {
              javascriptEnabled: true,
              relativeUrls: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    // Adds an error overlay whenever an error occurs in the application
    new ErrorOverlayPlugin(),
    // Add module names to factory functions so they appear in browser profiler.
    new webpack.NamedModulesPlugin(),
    // This is necessary to emit hot updates
    new webpack.HotModuleReplacementPlugin(),
    // Watcher doesn't work well if you mistype casing in a path so we use
    // a plugin that prints an error when you attempt to do this.
    // See https://github.com/facebookincubator/create-react-app/issues/240
    new CaseSensitivePathsPlugin(),
    // Moment.js is an extremely popular library that bundles large locale files
    // by default due to how Webpack interprets its code. This is a practical
    // solution that requires the user to opt into importing specific locales.
    // https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
    // You can remove this if you don't use Moment.js:
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    // Notifies user about build success/failure
    new WebpackNotifierPlugin({ title: "Webpack", contentImage: path.join(__dirname, "funkis_framework.png") }),
  ],
  devServer: {
    contentBase: "./",
    hot: true,
  },
});
