# {%= name %} change log

## Unreleased

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.8.0] - 1999-12-31

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

---

## What's the point of a change log?

To make it easier for users and contributors to see precisely what notable changes have been made between each release (or version) of the project.

## Each version should:

* List its release date in the format YYYY-MM-DD.
* Group changes to describe their impact on the project, as follows: - `Added` for new features. - `Changed` for changes in existing functionality. - `Deprecated` for once-stable features removed in upcoming releases. - `Removed` for deprecated features removed in this release. - `Fixed` for any bug fixes. - `Security` to invite users to upgrade in case of vulnerabilities.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
